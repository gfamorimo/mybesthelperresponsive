/**
 * Created by gfamorimo on 5/21/16.
 */
angular.module('myBestHelper').controller("ProfileController", function(UserService, $rootScope, toaster, $location) {

  var vm = this;

  vm.init = function(){
    if($rootScope.currentUser){
      vm.user = UserService.get($rootScope.currentUser);

      vm.profiles = UserService.getProfiles();

      processUserAnswers(vm.user.answers);

      UserService.getMatches().then(function(responseMatches){
        if(responseMatches.status == 200){
          vm.matchesProfiles = responseMatches.data.results;
        }else{
          vm.matchesProfiles = [];
        }
      });
    }else{
      toaster.pop('error', "Please, fill your informations first!");

      $location.path('/signup');
    }
  }

  function processUserAnswers(answers){
    var result = _.countBy(answers, function(answer){
      return (answer.profileId == 1)
        ?addAnswerToProfile(1):(answer.profileId == 2)
        ?addAnswerToProfile(2):(answer.profileId == 3)
        ?addAnswerToProfile(3):addAnswerToProfile(4);
    });

    vm.profile = _.max(vm.profiles, function(profile){return profile.answersMatches});
  }

  function addAnswerToProfile(profileId){
    var profile = _.findWhere(vm.profiles, {id: profileId});

    profile.answersMatches++;
  }

  vm.init();

});
