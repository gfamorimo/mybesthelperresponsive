/**
 * Created by gfamorimo on 5/21/16.
 */
angular.module('myBestHelper').controller("QuestionController", function($rootScope, $routeParams, $location, QuestionService, toaster, UserService) {

  // $stateParams.userId;
  var vm = this;

  vm.init = function(){
    if($rootScope.currentUser){
      vm.questionId = parseInt($routeParams.questionId);

      vm.question = QuestionService.get(parseInt(vm.questionId));
    }else{
      toaster.pop('error', "Please, fill your informations first!");

      $location.path('/signup');
    }
  }

  vm.goToQuestion = function(){
    if(validate()){
      if(QuestionService.count() > parseInt(vm.questionId)){
        save();

        vm.questionId++;
        $location.path('/question/' + vm.questionId);
      }else{
        $location.path('/profile');
      }
    }else{
      toaster.pop('error', "Please, answer the question!");
    }
  }

  vm.backToQuestion = function(){
    if(parseInt(vm.questionId) > 1){
      vm.questionId--;
      $location.path('/question/' + vm.questionId);
    }
  }

  vm.setAnswer = function(idAnswer, idProfile){
    vm.idAnswer = idAnswer;
    vm.idProfile = idProfile;
  }

  function validate(){
    var valid = true;

    if(!vm.idAnswer){
      valid = false;
    }

    return valid;
  }

  function save(){
    var user = UserService.get($rootScope.currentUser);

    user.answers.push({questionId: vm.questionId, answerId: vm.idAnswer, profileId: vm.idProfile});

    UserService.persist(user);
  }

  vm.init();

});
