/**
 * Created by gfamorimo on 5/21/16.
 */
angular.module('myBestHelper').controller("SignUpController", function($rootScope, UserService, $location, toaster) {

  var vm = this;

  vm.init = function(){
    $rootScope.questionId = 0;

    vm.user = {
      firstName: null,
      lastName: null,
      email: null,
      phoneNumber: null,
      password: null,
      answers: []
    };
  }

  vm.goToQuestion = function(){
    if(validate(vm.user)){
      UserService.persist(vm.user);

      $rootScope.questionId++;

      $rootScope.currentUser = vm.user.email;

      $location.path('/question/' + $rootScope.questionId);
    }else{
      toaster.pop('error', "Please, fill your information!");
    }
  }

  function validate(user){
    var valid = true;
    if(user){
      if(!user.firstName){
        valid = false;
      }
      if(!user.lastName){
        valid = false;
      }
      if(!user.email){
        valid = false;
      }
      if(!user.password){
        valid = false;
      }
    }else{
      valid = false;
    }

    return valid;
  }

  vm.init();

});
