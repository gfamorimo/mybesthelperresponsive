$(".change-colors, a.close-colors").on("click", function(e){
    $(".color-menu").toggle("fast");
});
$(".ruby a").on("click", function(e){
    $("body").attr('class', 'ruby-color-scheme ng-scope');
});
$(".greytones a").on("click", function(e){
    $("body").attr('class', 'greytones-color-scheme ng-scope');
});
$(".mango a").on("click", function(e){
    $("body").attr('class', 'mango-color-scheme ng-scope');
});
$(".ultramarine a").on("click", function(e){
    $("body").attr('class', 'ultramarine-color-scheme ng-scope');
});