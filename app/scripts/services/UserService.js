/**
 * Created by gfamorimo on 5/21/16.
 */
angular.module('myBestHelper').factory('UserService', function(localStorageService, $http){

  var profiles = [
    {id:1, title: 'Happiest Helper', description: 'I bring joy and fun into every situation. I love people and am thrilled to know I can make their lives better.', answersMatches: 0, image: 'images/happy.gif'},
    {id:2, title: 'Get things done', description: 'Energetic multitasker, I am the person everyone looks to when they want to make sure all is handled! ', answersMatches: 0, image: 'images/multitask.gif'},
    {id:3, title: 'Most reliable', description: 'All friends  always turn to me as I am like a rock - I will always be there for them and help them out.', answersMatches: 0, image: 'images/reliable.gif'},
    {id:4, title: 'Like a genie', description: 'I just know what needs to be done, and do it! No instructions required, when I am in charge, everything is better.', answersMatches: 0, image: 'images/genius.gif'}
  ];

  function persist(user){
    if(!get(user.email)){
      localStorageService.set('u-'+user.email, user);
    }else{
      remove(user.email);

      localStorageService.set('u-'+user.email, user);
    }
  }

  function get(email){
    return localStorageService.get('u-'+email);
  }

  function remove(email){
    localStorageService.remove('u-'+email);
  }

  function getProfiles(){
    return profiles;
  }

  function getMatches(){
    return $http.get('http://api.randomuser.me/?results=6');
  }

  return {
    persist: persist,
    get: get,
    remove: remove,
    getProfiles: getProfiles,
    getMatches: getMatches
  };

});
