/**
 * Created by gfamorimo on 5/21/16.
 */
angular.module('myBestHelper').factory('QuestionService', function(localStorageService){

  var questions = [
    {id: 1, question: 'What work are you most interested in?', answers: [
      {id:1, answer: 'Child care', icon:'lnr lnr-magic-wand', profileId: 1},
      {id:2, answer: 'Elder care', icon:'lnr lnr-mustache', profileId: 3},
      {id:3, answer: 'Home care', icon:'lnr lnr-home', profileId: 4},
      {id:4, answer: 'Pet care', icon:'lnr lnr-paw', profileId: 2}
    ]},
    {id: 2, question: 'Do you want to work?', answers: [
      {id:1, answer: 'Full-time', icon:'', profileId: 2},
      {id:2, answer: 'Part-time', icon:'', profileId: 3},
      {id:3, answer: 'Occasional', icon:'', profileId: 1},
      {id:4, answer: 'On-call', icon:'', profileId: 4}
    ]},
    {id: 3, question: 'Which person are you most like?', answers: [
      {id:1, answer: 'Gets things done', icon:'glyphicon glyphicon-ok', profileId: 2},
      {id:2, answer: 'Always happy', icon:'lnr lnr-smile', profileId: 1},
      {id:3, answer: 'Friendly and fun', icon:'lnr lnr-thumbs-up', profileId: 3},
      {id:4, answer: 'Professional', icon:'glyphicon glyphicon-briefcase', profileId: 4}
    ]},
    {id: 4, question: 'Your favorite music is:', answers: [
      {id:1, answer: 'Modern pop', icon:'', profileId: 1},
      {id:2, answer: 'Rock and roll', icon:'', profileId: 3},
      {id:3, answer: 'Classic', icon:'', profileId: 2},
      {id:4, answer: 'World music', icon:'', profileId: 4}
    ]},
    {id: 5, question: 'I would be most happy:', answers: [
      {id:1, answer: 'Cooking a meal', icon:'lnr lnr-dinner', profileId: 2},
      {id:2, answer: 'Taking a walk', icon:'lnr lnr-bicycle', profileId: 3},
      {id:3, answer: 'Arts and Crafts', icon:'lnr lnr-camera-video', profileId: 4},
      {id:4, answer: 'Playing sports', icon:'glyphicon glyphicon-tower', profileId: 1}
    ]},
    {id: 6, question: 'The coolest thing in this list is:', answers: [
      {id:1, answer: 'Gardening', icon:'', profileId: 2},
      {id:2, answer: 'Play an instrument', icon:'', profileId: 4},
      {id:3, answer: 'Travelling', icon:'', profileId: 3},
      {id:4, answer: 'Seeing a show', icon:'', profileId: 1}
    ]},
    {id: 7, question: 'I prefer spending time with people who are:', answers: [
      {id:1, answer: 'Easy going', icon:'', profileId: 3},
      {id:2, answer: 'Super smart', icon:'', profileId: 4},
      {id:3, answer: 'Open and direct', icon:'', profileId: 2},
      {id:4, answer: 'Laugh a lot ', icon:'', profileId: 1}
    ]},
    {id: 8, question: 'I am great at:', answers: [
      {id:1, answer: 'Listening to instructions', icon:'', profileId: 4},
      {id:2, answer: 'Problem solving', icon:'', profileId: 2},
      {id:3, answer: 'Using my experience', icon:'', profileId: 3},
      {id:4, answer: 'Learning from others', icon:'', profileId: 1}
    ]},
    {id: 9, question: 'I relax by:', answers: [
      {id:1, answer: 'Reading books', icon:'', profileId: 4},
      {id:2, answer: 'Doing something active', icon:'', profileId: 2},
      {id:3, answer: 'Hanging out with friends', icon:'', profileId: 3},
      {id:4, answer: 'Watching movies', icon:'', profileId: 1}
    ]},
    {id: 10, question: 'When I choose presents for friends?', answers: [
      {id:1, answer: 'Easy - I have lots of ideas', icon:'', profileId: 4},
      {id:2, answer: 'I plan long ahead', icon:'', profileId: 3},
      {id:3, answer: 'I am never sure', icon:'', profileId: 2},
      {id:4, answer: 'I am totally last minute', icon:'', profileId: 1}
    ]}
  ];

  function get(id){
    var q = null;

    angular.forEach(questions, function(question){
      if(question.id === id){
        q = question;
      }
    });

    return q;
  }

  function count(){
    return questions.length;
  }

  return {
    get: get,
    count: count
  }

});
