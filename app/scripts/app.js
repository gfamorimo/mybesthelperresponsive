'use strict';

/**
 * @ngdoc overview
 * @name myBestHelperApp
 * @description
 * # myBestHelperApp
 *
 * Main module of the application.
 */
angular
  .module('myBestHelper', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule',
    'toaster'
  ])
  .config(function ($routeProvider, localStorageServiceProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController',
        controllerAs: 'homeCtrl'
      })
      .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignUpController',
        controllerAs: 'signCtrl'
      })
      .when('/question/:questionId', {
        templateUrl: 'views/question.html',
        controller: 'QuestionController',
        controllerAs: 'questionCtrl'
      })
      .when('/newQuetion', {
        templateUrl: 'views/newQuetion.html',
        controller: 'NewQuestionController',
        controllerAs: 'newQuestionCtrl'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileController',
        controllerAs: 'profileCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    localStorageServiceProvider
      .setPrefix('my-best-helper');
  });
